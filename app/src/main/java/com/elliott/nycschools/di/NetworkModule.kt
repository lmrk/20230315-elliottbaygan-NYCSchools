package com.elliott.nycschools.di

import com.elliott.nycschools.network.retrofit.RetrofitService
import org.koin.dsl.module

val networkModule = module {
    single { RetrofitService() }
}