package com.elliott.nycschools

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.elliott.nycschools.constant.NavArguments.SCHOOL_NAME
import com.elliott.nycschools.constant.NavRoutes
import com.elliott.nycschools.ui.screen.SchoolInfoScreen
import com.elliott.nycschools.ui.screen.SchoolListScreen
import com.elliott.nycschools.ui.theme.NYCSchoolsTheme

class MainActivity : ComponentActivity() {
    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        installSplashScreen()
        setContent {
            val navController = rememberNavController()

            val (canPop, setCanPop) = remember { mutableStateOf(false) }
            navController.addOnDestinationChangedListener { controller, _, _ ->
                setCanPop(controller.previousBackStackEntry != null)
            }
            val navBackStackEntry by navController.currentBackStackEntryAsState()
            val currentDestination = navBackStackEntry?.destination?.route
            Surface(
                modifier = Modifier.fillMaxSize(),
                color = MaterialTheme.colorScheme.background
            ) {
                Scaffold(
                    topBar = {
                        // TODO : change title when navigating to another screen
                        val title = when (currentDestination) {
                            NavRoutes.MAIN_SCREEN -> stringResource(id = R.string.text_nyc_school_list)
                            NavRoutes.SCHOOL_INFO_SCREEN -> stringResource(id = R.string.text_high_school_info)
                            else -> {
                                stringResource(id = R.string.text_nyc_school_list)
                            }
                        }
                        TopAppBar(
                            title = {
                                Text(
                                    text = title,
                                    Modifier.fillMaxWidth(),
                                    textAlign = TextAlign.Center
                                )
                            },
                            colors = TopAppBarDefaults.smallTopAppBarColors(
                                containerColor = MaterialTheme.colorScheme.onPrimaryContainer,
                                titleContentColor = MaterialTheme.colorScheme.secondaryContainer,
                            ),
                            navigationIcon = {
                                if (canPop) {
                                    IconButton(
                                        onClick = { navController.navigateUp() },
                                        Modifier.size(20.dp)
                                    ) {
                                        Icon(
                                            imageVector = Icons.Filled.ArrowBack,
                                            tint = MaterialTheme.colorScheme.secondaryContainer,
                                            contentDescription = stringResource(R.string.content_desc_back)
                                        )
                                    }
                                } else {
                                    null
                                }
                            }
                        )
                    },
                    content = { innerPadding ->
                        Box(modifier = Modifier.padding(innerPadding)) {
                            MainScreen(navController)
                        }
                    }
                )
            }


        }
    }
}

@Composable
fun MainScreen(navController: NavHostController) {
    NavHost(
        navController,
        startDestination = NavRoutes.MAIN_SCREEN
    ) {
        composable(NavRoutes.MAIN_SCREEN) {
            SchoolListScreen(navController)
        }
        composable("${NavRoutes.SCHOOL_INFO_SCREEN}/{$SCHOOL_NAME}") { backStackEntry ->
            val schoolName = backStackEntry.arguments?.getString(SCHOOL_NAME)
            if (schoolName != null) {
                SchoolInfoScreen(schoolName)
            } else {
                // I'd put in a something went wr
                Text(stringResource(R.string.text_something_went_wrong))
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    NYCSchoolsTheme {
    }
}