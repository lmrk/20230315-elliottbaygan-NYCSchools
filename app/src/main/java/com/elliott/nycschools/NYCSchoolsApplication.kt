package com.elliott.nycschools

import android.app.Application
import com.elliott.nycschools.di.networkModule
import com.elliott.nycschools.di.repositoryModule
import com.elliott.nycschools.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.component.KoinComponent
import org.koin.core.context.startKoin

class NYCSchoolsApplication : Application(), KoinComponent {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@NYCSchoolsApplication)
            modules(viewModelModule)
            modules(networkModule)
            modules(repositoryModule)
        }
    }

}