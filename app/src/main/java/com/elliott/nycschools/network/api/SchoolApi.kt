package com.elliott.nycschools.network.api

import com.elliott.nycschools.network.constant.QueryParams.SCHOOL_NAME
import com.elliott.nycschools.network.constant.QueryParams.SELECT
import com.elliott.nycschools.network.constant.URL.HIGH_SCHOOL_DATA_PATH
import com.elliott.nycschools.network.model.HighSchoolResponseItem
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface SchoolApi {
    @GET(HIGH_SCHOOL_DATA_PATH)
    suspend fun getHighSchools(
        @Query(SELECT) selectFields: String? = null,
        @Query(SCHOOL_NAME) schoolName: String? = null
    ): Response<List<HighSchoolResponseItem>>
}