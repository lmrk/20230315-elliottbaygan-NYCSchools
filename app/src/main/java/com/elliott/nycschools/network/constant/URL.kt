package com.elliott.nycschools.network.constant

object URL {
    const val BASE_URL = "https://data.cityofnewyork.us/resource/"
    const val HIGH_SCHOOL_DATA_PATH = "f9bf-2cp4.json"
}