package com.elliott.nycschools.network.retrofit

sealed class NetworkResult<T>(
    val meta: T? = null,
    val data: Any? = null,
    val responseCode: Int? = null
) {
    class Success<T>(data: T) : NetworkResult<T>(data)
    class Error<T>(data: Any, meta: T? = null, responseCode: Int?) : NetworkResult<T>(meta, data, responseCode)
}