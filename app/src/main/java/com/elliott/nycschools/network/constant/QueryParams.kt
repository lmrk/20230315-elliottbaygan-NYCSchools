package com.elliott.nycschools.network.constant

object QueryParams {
    const val SELECT = "\$select"
    const val SCHOOL_NAME = "school_name"
}