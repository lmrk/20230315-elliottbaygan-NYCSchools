package com.elliott.nycschools.network.retrofit

import com.elliott.nycschools.network.constant.URL
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import java.util.concurrent.TimeUnit
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

class RetrofitService {
    fun getRetrofitClient(
        baselUrl: String = URL.BASE_URL
    ): Retrofit {
        val apiClient = OkHttpClient.Builder().apply {
            // timeouts
            connectTimeout(CONNECTION_TIMEOUT_SECONDS, TimeUnit.SECONDS)
            readTimeout(CONNECTION_TIMEOUT_SECONDS, TimeUnit.SECONDS)
            writeTimeout(CONNECTION_TIMEOUT_SECONDS, TimeUnit.SECONDS)

            // credentials
            addInterceptor(createOkHttpInterceptor())

            // content-type header
            addInterceptor(contentTypeUrlInterceptor)

            // logging
            addInterceptor(loggingInterceptor)
        }.build()

        // JSON converter
        val moshi = Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()

        val converterFactory = MoshiConverterFactory.create(moshi)

        val retrofit = Retrofit.Builder().also {
            it.baseUrl(baselUrl)
            it.client(apiClient)
            it.addConverterFactory(converterFactory)
        }.build()

        return retrofit
    }

    companion object {
        private const val CONNECTION_TIMEOUT_SECONDS: Long = 60
        private const val HEADER_CONTENT_TYPE = "Content-Type"
        private const val MIME_URL = "application/json"

        private val contentTypeUrlInterceptor = Interceptor { chain ->
            val request = chain.request().newBuilder()
                .addHeader(HEADER_CONTENT_TYPE, MIME_URL)
                .build()

            chain.proceed(request)
        }

        private val loggingInterceptor = HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }
        fun createOkHttpInterceptor(credentialHeaders: Map<String, String> = emptyMap()) =
            Interceptor { chain ->
                chain.request().newBuilder().also { request ->
                    credentialHeaders.forEach { (name, value) ->
                        request.addHeader(name, value)
                    }
                }.build().let { request ->
                    chain.proceed(request)
                }
            }
    }
}