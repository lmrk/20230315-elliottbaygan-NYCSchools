package com.elliott.nycschools.network.repository

import com.elliott.nycschools.network.api.SchoolApi
import com.elliott.nycschools.network.model.BaseApiResponse
import com.elliott.nycschools.network.model.HighSchoolResponseItem
import com.elliott.nycschools.network.retrofit.NetworkResult
import com.elliott.nycschools.network.retrofit.RetrofitService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import org.koin.core.component.KoinComponent

class HighSchoolRepository(private val retrofitServices: RetrofitService) : BaseApiResponse(),
    KoinComponent {

    suspend fun getHighSchoolNames(): Flow<NetworkResult<List<HighSchoolResponseItem>>> {
        return flow {
            emit(safeApiCall {
                retrofitServices.getRetrofitClient().create(SchoolApi::class.java).getHighSchools("school_name")
            })
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getHighSchoolDataByName(schoolName: String): Flow<NetworkResult<List<HighSchoolResponseItem>>> {
        return flow {
            emit(safeApiCall {
                retrofitServices.getRetrofitClient().create(SchoolApi::class.java).getHighSchools(schoolName = schoolName)
            })
        }.flowOn(Dispatchers.IO)
    }

}