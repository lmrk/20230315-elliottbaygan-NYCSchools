package com.elliott.nycschools.util

import java.net.URLEncoder
import java.nio.charset.StandardCharsets

fun String.encodeUTF8(): String {
    return URLEncoder.encode(this, StandardCharsets.UTF_8.toString()).replace("+", " ")
}