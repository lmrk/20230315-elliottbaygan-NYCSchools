package com.elliott.nycschools.ui.screen

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Card
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.elliott.nycschools.R
import com.elliott.nycschools.network.retrofit.CallStateContent
import com.elliott.nycschools.ui.component.SchoolInfoCard
import com.elliott.nycschools.ui.viewmodel.SchoolInfoViewModel
import org.koin.androidx.compose.getViewModel

@Composable
fun SchoolInfoScreen(schoolName: String) {
    val schoolInfoViewModel = getViewModel<SchoolInfoViewModel>()

    val highSchoolNameList by schoolInfoViewModel.highSchool.collectAsState()

    Column {
        CallStateContent(
            highSchoolNameList,
            loadingUI = {
                CircularProgressIndicator(
                    Modifier
                        .fillMaxSize()
                        .padding(60.dp)
                )
            },
            successUI = {
                Box {
                    // Composable content to be faded in
                    Card(Modifier.padding(40.dp)) {
                        if (it != null) {
                            SchoolInfoCard(it)
                        }
                    }
                }
            },
            emptyUI = {
                Text(stringResource(R.string.text_empty_error))
            },
            errorUI = {
                // Would have a network error screen, as well as a try again button
            }
        )
    }


    LaunchedEffect(true) {
        schoolInfoViewModel.getSchoolInfo(schoolName)
    }
}