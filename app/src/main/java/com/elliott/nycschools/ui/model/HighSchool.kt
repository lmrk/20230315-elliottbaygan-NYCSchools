package com.elliott.nycschools.ui.model

data class HighSchool(
    val name: String?,
    val numOfSatTestTakers: String? = "",
    val satCriticalReadingAvgScore: String? = "",
    val satMathAvgScore: String? = "",
    val satWritingAvgScore: String? = "",
)
