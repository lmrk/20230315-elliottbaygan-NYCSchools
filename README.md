--NYCSchool App--
Use this application to learn about the SAT scores of students of New York High Schools! Scroll through the alphabetized list of School names and pick out the one you want.
Useful statistics for SATs will be displayed for your school of choice.
All school related data comes from https://dev.socrata.com/foundry/data.cityofnewyork.us/f9bf-2cp4.

Implementation
The application is built using Kotlin Compose, and Jetpack navigation for the UI/navigation.
There are some unit tests for some of the business logic, one file using Mockk for mocking coroutine responses, and the other using Mockito.
It follows MVVM, uses Koin for DI. I'm using retrofit and the repository pattern for the network layer.

What I would do with more time
1) Create a release and debug flavor. I'd make HighSchoolRepository an interface, and have two implementations (one for testing and one for release).
2) Abstract HighSchoolRepository's data service provider dependency so that the TestHighSchoolRepository will use a test data provider (Loaded from debug's source set), and the other one will use retrofit.
3) Have koin inject the mocked data repository from debug source when in debug, inject the live repository when in release
4) Fill out the error screens, make the error handling more uniform within the app than it is. Includes status code handling.
5) Add an alphabetic side scrollbar
6) Write a test to make sure the list of school names is alphabetized
7) Add animations between screens
8) Change the loading animation of the High school info to the same shimmering effect as the one that the list has